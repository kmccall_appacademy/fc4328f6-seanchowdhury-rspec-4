class Timer
  def seconds
    @seconds = 0
  end

  def seconds=(input)
    @seconds = input
  end

  def time_string
    hours = @seconds / 3600
    minutes = (@seconds - (hours * 3600)) / 60
    seconds_final = (@seconds - (hours * 3600) - (minutes * 60))
    return "#{padd(hours)}:#{padd(minutes)}:#{padd(seconds_final)}"
  end

  def padd(text)
    return text.to_s if text > 10
    "0#{text}"
  end
end
