class Dictionary
  def initialize
    @entries = {}
  end

  def entries
    @entries
  end

  def keywords
    @entries.keys.sort
  end

  def add(entry)
    if entry.is_a?(::Hash)
      @entries.merge!(entry)
    else
      @entries[entry] = nil
    end
  end

  def include?(word)
    self.keywords.include?(word)
  end

  def find(word)
    @entries.select {|key,value| key.start_with?(word)}
  end

  def printable
    output = []
    self.keywords.each_with_index do |word,idx|
      if idx == 0
        output << "[#{word}] \"#{@entries[word]}\""
      else
        output << "\n[#{word}] \"#{@entries[word]}\""
      end
    end
    output.join
  end

end
