class Temperature
  def initialize(temp = {})
    @temp = temp
  end

  def in_fahrenheit
    return @temp[:f] if @temp.has_key?(:f)
    return @temp[:c] * (9/5.to_f) + 32
  end

  def in_celsius
    return @temp[:c] if @temp.has_key?(:c)
    return (@temp[:f] - 32) * 5/9.to_f
  end

  def self.from_celsius(input)
    self.new(:c => input)
  end

  def self.from_fahrenheit(input)
    self.new(:f => input)
  end
end

class Celsius < Temperature
  def initialize(temp)
    super(:c => temp)
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    super(:f => temp)
  end
end
