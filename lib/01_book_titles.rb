class Book
  def title
    @title
  end

  def title=(input)
    @title = input.split(" ").map.with_index do |word,idx|
      if idx == 0
        word.capitalize
      elsif ["and", "or", "if","in","a","to","an","the","of"].any? { |el| el == word }
        word
      else
        word.capitalize
      end
    end.join(" ")
  end
end
